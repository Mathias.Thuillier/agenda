<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220622140801 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP INDEX IDX_2CEDC8777E3C61F9');
        $this->addSql('CREATE TEMPORARY TABLE __temp__agenda AS SELECT id, owner_id, name FROM agenda');
        $this->addSql('DROP TABLE agenda');
        $this->addSql('CREATE TABLE agenda (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, owner_id INTEGER DEFAULT NULL, name VARCHAR(255) NOT NULL, CONSTRAINT FK_2CEDC8777E3C61F9 FOREIGN KEY (owner_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO agenda (id, owner_id, name) SELECT id, owner_id, name FROM __temp__agenda');
        $this->addSql('DROP TABLE __temp__agenda');
        $this->addSql('CREATE INDEX IDX_2CEDC8777E3C61F9 ON agenda (owner_id)');
        $this->addSql('DROP INDEX IDX_B0B67439EA67784A');
        $this->addSql('DROP INDEX IDX_B0B67439A76ED395');
        $this->addSql('CREATE TEMPORARY TABLE __temp__agenda_user AS SELECT agenda_id, user_id FROM agenda_user');
        $this->addSql('DROP TABLE agenda_user');
        $this->addSql('CREATE TABLE agenda_user (agenda_id INTEGER NOT NULL, user_id INTEGER NOT NULL, PRIMARY KEY(agenda_id, user_id), CONSTRAINT FK_B0B67439EA67784A FOREIGN KEY (agenda_id) REFERENCES agenda (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE, CONSTRAINT FK_B0B67439A76ED395 FOREIGN KEY (user_id) REFERENCES "user" (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO agenda_user (agenda_id, user_id) SELECT agenda_id, user_id FROM __temp__agenda_user');
        $this->addSql('DROP TABLE __temp__agenda_user');
        $this->addSql('CREATE INDEX IDX_B0B67439EA67784A ON agenda_user (agenda_id)');
        $this->addSql('CREATE INDEX IDX_B0B67439A76ED395 ON agenda_user (user_id)');
        $this->addSql('DROP INDEX UNIQ_BE9448127E3C61F9');
        $this->addSql('CREATE TEMPORARY TABLE __temp__contact_detail AS SELECT id, owner_id FROM contact_detail');
        $this->addSql('DROP TABLE contact_detail');
        $this->addSql('CREATE TABLE contact_detail (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, owner_id INTEGER NOT NULL, CONSTRAINT FK_BE9448127E3C61F9 FOREIGN KEY (owner_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO contact_detail (id, owner_id) SELECT id, owner_id FROM __temp__contact_detail');
        $this->addSql('DROP TABLE __temp__contact_detail');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_BE9448127E3C61F9 ON contact_detail (owner_id)');
        $this->addSql('DROP INDEX IDX_6FBE9DA2B62120C0');
        $this->addSql('DROP INDEX IDX_6FBE9DA28486F9AC');
        $this->addSql('CREATE TEMPORARY TABLE __temp__contact_detail_adress AS SELECT contact_detail_id, adress_id FROM contact_detail_adress');
        $this->addSql('DROP TABLE contact_detail_adress');
        $this->addSql('CREATE TABLE contact_detail_adress (contact_detail_id INTEGER NOT NULL, adress_id INTEGER NOT NULL, PRIMARY KEY(contact_detail_id, adress_id), CONSTRAINT FK_6FBE9DA2B62120C0 FOREIGN KEY (contact_detail_id) REFERENCES contact_detail (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE, CONSTRAINT FK_6FBE9DA28486F9AC FOREIGN KEY (adress_id) REFERENCES adress (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO contact_detail_adress (contact_detail_id, adress_id) SELECT contact_detail_id, adress_id FROM __temp__contact_detail_adress');
        $this->addSql('DROP TABLE __temp__contact_detail_adress');
        $this->addSql('CREATE INDEX IDX_6FBE9DA2B62120C0 ON contact_detail_adress (contact_detail_id)');
        $this->addSql('CREATE INDEX IDX_6FBE9DA28486F9AC ON contact_detail_adress (adress_id)');
        $this->addSql('DROP INDEX IDX_15086EB0B62120C0');
        $this->addSql('DROP INDEX IDX_15086EB03B7323CB');
        $this->addSql('CREATE TEMPORARY TABLE __temp__contact_detail_phone AS SELECT contact_detail_id, phone_id FROM contact_detail_phone');
        $this->addSql('DROP TABLE contact_detail_phone');
        $this->addSql('CREATE TABLE contact_detail_phone (contact_detail_id INTEGER NOT NULL, phone_id INTEGER NOT NULL, PRIMARY KEY(contact_detail_id, phone_id), CONSTRAINT FK_15086EB0B62120C0 FOREIGN KEY (contact_detail_id) REFERENCES contact_detail (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE, CONSTRAINT FK_15086EB03B7323CB FOREIGN KEY (phone_id) REFERENCES phone (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO contact_detail_phone (contact_detail_id, phone_id) SELECT contact_detail_id, phone_id FROM __temp__contact_detail_phone');
        $this->addSql('DROP TABLE __temp__contact_detail_phone');
        $this->addSql('CREATE INDEX IDX_15086EB0B62120C0 ON contact_detail_phone (contact_detail_id)');
        $this->addSql('CREATE INDEX IDX_15086EB03B7323CB ON contact_detail_phone (phone_id)');
        $this->addSql('DROP INDEX IDX_52D62DDFB62120C0');
        $this->addSql('DROP INDEX IDX_52D62DDF2288CAE');
        $this->addSql('CREATE TEMPORARY TABLE __temp__contact_detail_electronic AS SELECT contact_detail_id, electronic_id FROM contact_detail_electronic');
        $this->addSql('DROP TABLE contact_detail_electronic');
        $this->addSql('CREATE TABLE contact_detail_electronic (contact_detail_id INTEGER NOT NULL, electronic_id INTEGER NOT NULL, PRIMARY KEY(contact_detail_id, electronic_id), CONSTRAINT FK_52D62DDFB62120C0 FOREIGN KEY (contact_detail_id) REFERENCES contact_detail (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE, CONSTRAINT FK_52D62DDF2288CAE FOREIGN KEY (electronic_id) REFERENCES electronic (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO contact_detail_electronic (contact_detail_id, electronic_id) SELECT contact_detail_id, electronic_id FROM __temp__contact_detail_electronic');
        $this->addSql('DROP TABLE __temp__contact_detail_electronic');
        $this->addSql('CREATE INDEX IDX_52D62DDFB62120C0 ON contact_detail_electronic (contact_detail_id)');
        $this->addSql('CREATE INDEX IDX_52D62DDF2288CAE ON contact_detail_electronic (electronic_id)');
        $this->addSql('DROP INDEX IDX_1E6F305EE4DFAB75');
        $this->addSql('DROP INDEX IDX_1E6F305EA832C1C9');
        $this->addSql('CREATE TEMPORARY TABLE __temp__electronic AS SELECT id, email_id, websites_id FROM electronic');
        $this->addSql('DROP TABLE electronic');
        $this->addSql('CREATE TABLE electronic (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, email_id INTEGER DEFAULT NULL, websites_id INTEGER DEFAULT NULL, CONSTRAINT FK_1E6F305EA832C1C9 FOREIGN KEY (email_id) REFERENCES emails (id) NOT DEFERRABLE INITIALLY IMMEDIATE, CONSTRAINT FK_1E6F305EE4DFAB75 FOREIGN KEY (websites_id) REFERENCES websites (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO electronic (id, email_id, websites_id) SELECT id, email_id, websites_id FROM __temp__electronic');
        $this->addSql('DROP TABLE __temp__electronic');
        $this->addSql('CREATE INDEX IDX_1E6F305EE4DFAB75 ON electronic (websites_id)');
        $this->addSql('CREATE INDEX IDX_1E6F305EA832C1C9 ON electronic (email_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP INDEX IDX_2CEDC8777E3C61F9');
        $this->addSql('CREATE TEMPORARY TABLE __temp__agenda AS SELECT id, owner_id, name FROM agenda');
        $this->addSql('DROP TABLE agenda');
        $this->addSql('CREATE TABLE agenda (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, owner_id INTEGER DEFAULT NULL, name VARCHAR(255) NOT NULL)');
        $this->addSql('INSERT INTO agenda (id, owner_id, name) SELECT id, owner_id, name FROM __temp__agenda');
        $this->addSql('DROP TABLE __temp__agenda');
        $this->addSql('CREATE INDEX IDX_2CEDC8777E3C61F9 ON agenda (owner_id)');
        $this->addSql('DROP INDEX IDX_B0B67439EA67784A');
        $this->addSql('DROP INDEX IDX_B0B67439A76ED395');
        $this->addSql('CREATE TEMPORARY TABLE __temp__agenda_user AS SELECT agenda_id, user_id FROM agenda_user');
        $this->addSql('DROP TABLE agenda_user');
        $this->addSql('CREATE TABLE agenda_user (agenda_id INTEGER NOT NULL, user_id INTEGER NOT NULL, PRIMARY KEY(agenda_id, user_id))');
        $this->addSql('INSERT INTO agenda_user (agenda_id, user_id) SELECT agenda_id, user_id FROM __temp__agenda_user');
        $this->addSql('DROP TABLE __temp__agenda_user');
        $this->addSql('CREATE INDEX IDX_B0B67439EA67784A ON agenda_user (agenda_id)');
        $this->addSql('CREATE INDEX IDX_B0B67439A76ED395 ON agenda_user (user_id)');
        $this->addSql('DROP INDEX UNIQ_BE9448127E3C61F9');
        $this->addSql('CREATE TEMPORARY TABLE __temp__contact_detail AS SELECT id, owner_id FROM contact_detail');
        $this->addSql('DROP TABLE contact_detail');
        $this->addSql('CREATE TABLE contact_detail (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, owner_id INTEGER NOT NULL)');
        $this->addSql('INSERT INTO contact_detail (id, owner_id) SELECT id, owner_id FROM __temp__contact_detail');
        $this->addSql('DROP TABLE __temp__contact_detail');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_BE9448127E3C61F9 ON contact_detail (owner_id)');
        $this->addSql('DROP INDEX IDX_6FBE9DA2B62120C0');
        $this->addSql('DROP INDEX IDX_6FBE9DA28486F9AC');
        $this->addSql('CREATE TEMPORARY TABLE __temp__contact_detail_adress AS SELECT contact_detail_id, adress_id FROM contact_detail_adress');
        $this->addSql('DROP TABLE contact_detail_adress');
        $this->addSql('CREATE TABLE contact_detail_adress (contact_detail_id INTEGER NOT NULL, adress_id INTEGER NOT NULL, PRIMARY KEY(contact_detail_id, adress_id))');
        $this->addSql('INSERT INTO contact_detail_adress (contact_detail_id, adress_id) SELECT contact_detail_id, adress_id FROM __temp__contact_detail_adress');
        $this->addSql('DROP TABLE __temp__contact_detail_adress');
        $this->addSql('CREATE INDEX IDX_6FBE9DA2B62120C0 ON contact_detail_adress (contact_detail_id)');
        $this->addSql('CREATE INDEX IDX_6FBE9DA28486F9AC ON contact_detail_adress (adress_id)');
        $this->addSql('DROP INDEX IDX_52D62DDFB62120C0');
        $this->addSql('DROP INDEX IDX_52D62DDF2288CAE');
        $this->addSql('CREATE TEMPORARY TABLE __temp__contact_detail_electronic AS SELECT contact_detail_id, electronic_id FROM contact_detail_electronic');
        $this->addSql('DROP TABLE contact_detail_electronic');
        $this->addSql('CREATE TABLE contact_detail_electronic (contact_detail_id INTEGER NOT NULL, electronic_id INTEGER NOT NULL, PRIMARY KEY(contact_detail_id, electronic_id))');
        $this->addSql('INSERT INTO contact_detail_electronic (contact_detail_id, electronic_id) SELECT contact_detail_id, electronic_id FROM __temp__contact_detail_electronic');
        $this->addSql('DROP TABLE __temp__contact_detail_electronic');
        $this->addSql('CREATE INDEX IDX_52D62DDFB62120C0 ON contact_detail_electronic (contact_detail_id)');
        $this->addSql('CREATE INDEX IDX_52D62DDF2288CAE ON contact_detail_electronic (electronic_id)');
        $this->addSql('DROP INDEX IDX_15086EB0B62120C0');
        $this->addSql('DROP INDEX IDX_15086EB03B7323CB');
        $this->addSql('CREATE TEMPORARY TABLE __temp__contact_detail_phone AS SELECT contact_detail_id, phone_id FROM contact_detail_phone');
        $this->addSql('DROP TABLE contact_detail_phone');
        $this->addSql('CREATE TABLE contact_detail_phone (contact_detail_id INTEGER NOT NULL, phone_id INTEGER NOT NULL, PRIMARY KEY(contact_detail_id, phone_id))');
        $this->addSql('INSERT INTO contact_detail_phone (contact_detail_id, phone_id) SELECT contact_detail_id, phone_id FROM __temp__contact_detail_phone');
        $this->addSql('DROP TABLE __temp__contact_detail_phone');
        $this->addSql('CREATE INDEX IDX_15086EB0B62120C0 ON contact_detail_phone (contact_detail_id)');
        $this->addSql('CREATE INDEX IDX_15086EB03B7323CB ON contact_detail_phone (phone_id)');
        $this->addSql('DROP INDEX IDX_1E6F305EA832C1C9');
        $this->addSql('DROP INDEX IDX_1E6F305EE4DFAB75');
        $this->addSql('CREATE TEMPORARY TABLE __temp__electronic AS SELECT id, email_id, websites_id FROM electronic');
        $this->addSql('DROP TABLE electronic');
        $this->addSql('CREATE TABLE electronic (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, email_id INTEGER DEFAULT NULL, websites_id INTEGER DEFAULT NULL)');
        $this->addSql('INSERT INTO electronic (id, email_id, websites_id) SELECT id, email_id, websites_id FROM __temp__electronic');
        $this->addSql('DROP TABLE __temp__electronic');
        $this->addSql('CREATE INDEX IDX_1E6F305EA832C1C9 ON electronic (email_id)');
        $this->addSql('CREATE INDEX IDX_1E6F305EE4DFAB75 ON electronic (websites_id)');
    }
}
