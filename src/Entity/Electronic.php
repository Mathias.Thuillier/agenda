<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\ElectronicRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=ElectronicRepository::class)
 */
class Electronic
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Emails::class)
     */
    private $email;

    /**
     * @ORM\ManyToOne(targetEntity=Websites::class)
     */
    private $websites;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?Emails
    {
        return $this->email;
    }

    public function setEmail(?Emails $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getWebsite(): ?Websites
    {
        return $this->websites;
    }

    public function setWebsite(?Websites $websites): self
    {
        $this->websites = $websites;

        return $this;
    }
}
