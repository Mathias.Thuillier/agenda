<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use App\Controller\UserAgenda;
/**
 * @ApiResource(normalizationContext={"groups"={"read:user"}}, collectionOperations={"get", "post"},
 * itemOperations={"get",
 * "get_agenda"={
 *      "method"="get",
 *      "path"="/users/{id}/agenda",
 *      "controller"=UserAgenda::class,
 *      }, 
 * "get_adresses"={
 *      "method"="get",
 *      "path"="/users/{id}/adresses",
 *      "controller"=UserAdresses::class,
 *      }, 
 *  "delete",
 *  "patch",
 *  "put",
 * })
 * @ORM\Entity(repositoryClass=UserRepository::class)
 * @ORM\Table(name="`user`")
 */
class User
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"read:user"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"read:user"})
     */
    private $lastName;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"read:user"})
     */
    private $firstName;

    /**
     * @ORM\Column(type="date")
     * @Groups({"read:user"})
     */
    private $birthday;

    /**
     * @ORM\OneToMany(targetEntity=Agenda::class, mappedBy="owner")
     */
    private $agendas;

    /**
     * @ORM\ManyToMany(targetEntity=Agenda::class, mappedBy="users")
     */
    private $contactAgenda;

    /**
     * @ORM\OneToOne(targetEntity=ContactDetail::class, mappedBy="owner", cascade={"persist", "remove"})
     */
    private $contactDetail;

    public function __construct()
    {
        $this->agendas = new ArrayCollection();
        $this->contactAgenda = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getBirthday(): ?\DateTimeInterface
    {
        return $this->birthday;
    }

    public function setBirthday(\DateTimeInterface $birthday): self
    {
        $this->birthday = $birthday;

        return $this;
    }

    /**
     * @return Collection<int, Agenda>
     */
    public function getAgendas(): Collection
    {
        return $this->agendas;
    }

    public function addAgenda(Agenda $agenda): self
    {
        if (!$this->agendas->contains($agenda)) {
            $this->agendas[] = $agenda;
            $agenda->setOwner($this);
        }

        return $this;
    }

    public function removeAgenda(Agenda $agenda): self
    {
        if ($this->agendas->removeElement($agenda)) {
            // set the owning side to null (unless already changed)
            if ($agenda->getOwner() === $this) {
                $agenda->setOwner(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Agenda>
     */
    public function getContactAgenda(): Collection
    {
        return $this->contactAgenda;
    }

    public function addContactAgenda(Agenda $contactAgenda): self
    {
        if (!$this->contactAgenda->contains($contactAgenda)) {
            $this->contactAgenda[] = $contactAgenda;
            $contactAgenda->addUser($this);
        }

        return $this;
    }

    public function removeContactAgenda(Agenda $contactAgenda): self
    {
        if ($this->contactAgenda->removeElement($contactAgenda)) {
            $contactAgenda->removeUser($this);
        }

        return $this;
    }

    public function getContactDetail(): ?ContactDetail
    {
        return $this->contactDetail;
    }

    public function setContactDetail(ContactDetail $contactDetail): self
    {
        // set the owning side of the relation if necessary
        if ($contactDetail->getOwner() !== $this) {
            $contactDetail->setOwner($this);
        }

        $this->contactDetail = $contactDetail;

        return $this;
    }  
}
