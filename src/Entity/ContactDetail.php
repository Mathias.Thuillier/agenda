<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\ContactDetailRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource(normalizationContext={"groups"={"read:contactDetail"}}, collectionOperations={"get", "post"},
 * itemOperations={"get",
 * "get_adresses"={
 *      "method"="get",
 *      "path"="/contactDetails/{id}/adresses",
 *      "controller"=AgendaUsers::class,
 *      }, 
 * "get_adresses_id"={
 *      "method"="get",
 *      "path"="/contactDetails/{id}/adresses/{id2}",
 *      "controller"=UserAdresses::class,
 *      }, 
 * "get_phone"={
 *      "method"="get",
 *      "path"="/contactDetails/{id}/phones",
 *      "controller"=AgendaUsers::class,
 *      }, 
 * "get_phone_id"={
 *      "method"="get",
 *      "path"="/contactDetails/{id}/phones/{id2}",
 *      "controller"=UserAdresses::class,
 *      }, 
 * "get_electronics"={
 *      "method"="get",
 *      "path"="/contactDetails/{id}/electronics/{id2}",
 *      "controller"=UserAdresses::class,
 *      }, 
 * "get_electronics_email"={
 *      "method"="get",
 *      "path"="/contactDetails/{id}/electronics/emails",
 *      "controller"=UserAdresses::class,
 *      }, 
 * "get_electronics_email_id"={
 *      "method"="get",
 *      "path"="/contactDetails/{id}/electronics/emails/{id2}",
 *      "controller"=UserAdresses::class,
 *      }, 
 * "get_electronics_website"={
 *      "method"="get",
 *      "path"="/contactDetails/{id}/website",
 *      "controller"=UserAdresses::class,
 *      }, 
 * "get_electronics_website_id"={
 *      "method"="get",
 *      "path"="/contactDetails/{id}/electronics/website/{id2}",
 *      "controller"=UserAdresses::class,
 *      }, 
 *  "delete",
 *  "patch",
 *  "put",
 * })
 * @ORM\Entity(repositoryClass=ContactDetailRepository::class)
 */
class ContactDetail
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity=User::class, inversedBy="contactDetail", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $owner;

    /**
     * @ORM\ManyToMany(targetEntity=Adress::class)
     */
    private $adress;

    /**
     * @ORM\ManyToMany(targetEntity=Phone::class)
     */
    private $phone;

    /**
     * @ORM\ManyToMany(targetEntity=Electronic::class)
     */
    private $electronic;

    public function __construct()
    {
        $this->adress = new ArrayCollection();
        $this->phone = new ArrayCollection();
        $this->electronic = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getOwner(): ?User
    {
        return $this->owner;
    }

    public function setOwner(User $owner): self
    {
        $this->owner = $owner;

        return $this;
    }

    /**
     * @return Collection<int, Adress>
     */
    public function getAdress(): Collection
    {
        return $this->adress;
    }

    public function addAdress(Adress $adress): self
    {
        if (!$this->adress->contains($adress)) {
            $this->adress[] = $adress;
        }

        return $this;
    }

    public function removeAdress(Adress $adress): self
    {
        $this->adress->removeElement($adress);

        return $this;
    }

    /**
     * @return Collection<int, Phone>
     */
    public function getPhone(): Collection
    {
        return $this->phone;
    }

    public function addPhone(Phone $phone): self
    {
        if (!$this->phone->contains($phone)) {
            $this->phone[] = $phone;
        }

        return $this;
    }

    public function removePhone(Phone $phone): self
    {
        $this->phone->removeElement($phone);

        return $this;
    }

    /**
     * @return Collection<int, Electronic>
     */
    public function getElectronic(): Collection
    {
        return $this->electronic;
    }

    public function addElectronic(Electronic $electronic): self
    {
        if (!$this->electronic->contains($electronic)) {
            $this->electronic[] = $electronic;
        }

        return $this;
    }

    public function removeElectronic(Electronic $electronic): self
    {
        $this->electronic->removeElement($electronic);

        return $this;
    }
}
