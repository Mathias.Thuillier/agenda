<?php

declare(strict_types=1);

namespace App\Controller;
use App\Entity\User;
class UserAgenda
{
    public function __invoke(User $user): User
    {
        return $user->getAgendas();
    }
}