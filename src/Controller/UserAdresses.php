<?php

declare(strict_types=1);

namespace App\Controller;
use App\Entity\User;
class UserAdresses
{
    public function __invoke(User $user, User $data): User
    {
        return $user->getAdresses();
    }
}